import React, { useEffect, useState} from 'react';
import ReactDOM from 'react-dom';
import './index.scss'
import { IntlProvider } from 'react-intl';
import { Header } from "./components/Core/Header";
import { Main } from "./components/Main"
// Material UI 控制元件
import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles';

// 改變顏色主題
const theme = createMuiTheme({
    palette: {
        type: "dark",
    }
});


const Root = () => {
    // 多國語言i18n
    const [lang, setLang] = useState('en')
    const [locale, setLocale] = useState(undefined)

    // 切換語系使用
    useEffect(async() => {
        const resp = await fetch(`./lang/${ lang }.json`)
        const data = await resp.json()
        setLocale(data)
    },[lang])

    return (
        <ThemeProvider theme={theme}>
            <IntlProvider messages={locale} locale='en'>
                <Header lang={ lang } setLang={ setLang } />
                <Main />
            </IntlProvider>
        </ThemeProvider>
    );
};

ReactDOM.render(<Root />, document.getElementById('root'));

