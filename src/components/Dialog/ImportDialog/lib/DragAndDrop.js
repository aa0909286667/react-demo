// 基本
import React from "react";
import { FormattedMessage } from "react-intl";
// icon
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import SearchIcon from "@material-ui/icons/Search";
// button
import Button from "@material-ui/core/Button";
// import Excel
import InputFiles from "react-input-files";



const DragAndDrop = props => {

    const { data, dispatch } = props;

    const handleDragEnter = e => {
        e.preventDefault();
        e.stopPropagation();

        dispatch({ type: 'SET_DROP_DEPTH', dropDepth: data.dropDepth + 1 });
    };

    const handleDragLeave = e => {
        e.preventDefault();
        e.stopPropagation();
        dispatch({ type: 'SET_DROP_DEPTH', dropDepth: data.dropDepth - 1 });
        if (data.dropDepth > 0) return
        dispatch({ type: 'SET_IN_DROP_ZONE', inDropZone: false })
    };

    const handleDragOver = e => {
        e.preventDefault();
        e.stopPropagation();

        e.dataTransfer.dropEffect = 'copy';
        dispatch({ type: 'SET_IN_DROP_ZONE', inDropZone: true });
    };

    const handleDrop = e => {
        e.preventDefault();
        e.stopPropagation();

        let files = [...e.dataTransfer.files];

        if (files && files.length > 0) {
            const existingFiles = data.fileList.map(f => f.name)
            files = files.filter(f =>  !existingFiles.includes(f.name))
            dispatch({ type: 'ADD_FILE_TO_LIST', files });
            dispatch({ type: 'SET_DROP_DEPTH', dropDepth: 0 });
            dispatch({ type: 'SET_IN_DROP_ZONE', inDropZone: false });
        }
    };

    function onImportExcel(files) {
        files = [files[0]]
        if ( files[0] !== undefined) {
            const existingFiles = data.fileList.map(f => f.name)
            files = files.filter(f =>  !existingFiles.includes(f.name))
            dispatch({ type: 'ADD_FILE_TO_LIST', files });
            dispatch({ type: 'SET_DROP_DEPTH', dropDepth: 3 });
            dispatch({ type: 'SET_IN_DROP_ZONE', inDropZone: false });
        }
    }


    return (
        <div className="import-windows">
            <InputFiles
                accept=".xls,.csv,.xlsx"
                children={
                    <input
                        className="import-btn"
                        id="contained-button-file"
                        multiple
                        type="file"
                    />
                }
                onChange={ onImportExcel }
            />
            <label htmlFor="contained-button-file">
                <div className={data.inDropZone ? 'btn uploadBox drag-drop-zone inside-drag-area' : 'btn uploadBox drag-drop-zone'}
                     onDrop={e => handleDrop(e)}
                     onDragOver={e => handleDragOver(e)}
                     onDragEnter={e => handleDragEnter(e)}
                     onDragLeave={e => handleDragLeave(e)}
                >
                    <form className="fileUpload">
                        <CloudUploadIcon />
                        <h3 className="f-20">
                            <FormattedMessage
                                id="dialog.importDialog.uploadFiles"
                                defaultMessage="Drop Files here"
                            />
                        </h3>
                        <Button
                            variant="contained"
                            className="chooseFilesBtn"
                            component="span"
                            startIcon={ <SearchIcon className="searchIcon"/> }
                        >
                            <h3 className="f-16">
                                <FormattedMessage
                                    id="dialog.importDialog.chooseFiles"
                                    defaultMessage="Choose Files"
                                />
                            </h3>
                        </Button>
                    </form>
                </div>
            </label>
        </div>
    );
}

export default DragAndDrop;
