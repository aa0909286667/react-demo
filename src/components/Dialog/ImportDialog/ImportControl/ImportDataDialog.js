import React from "react";
import Button from "@material-ui/core/Button";
import {FormattedMessage, injectIntl} from "react-intl";
import { ImportDialog } from "../index";


const ImportDataDialogTemplate = props =>{

    const { openImportDialog, ImportDialogClose, ImportDialogOpen } = props;

    return(
        <div className="margin-top-bottom-40">
            <Button
                variant="contained"
                color="primary"
                onClick={ ImportDialogOpen }
            >
                <FormattedMessage
                    id="no_data.import_data"
                    defaultMessage="Import Data"
                />
            </Button>
            <ImportDialog
                openImportDialog={ openImportDialog }
                ImportDialogClose={ ImportDialogClose }
            />
        </div>
    )
}

const ImportDataDialog = injectIntl( ImportDataDialogTemplate );
export default ImportDataDialog;
