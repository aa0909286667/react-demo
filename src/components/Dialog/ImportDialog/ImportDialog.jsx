// 基本
import React from "react"
import {injectIntl, FormattedMessage} from 'react-intl';
// Button
import Button from '@material-ui/core/Button';
// Dialog 組件
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
// 步驟條
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
// DragAndDrop
import DragAndDrop from './lib/DragAndDrop'
import XLSX from "xlsx";
// Table
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from '@material-ui/core/TableBody';
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
// Select
import MenuItem from "@material-ui/core/MenuItem";
import ListSubheader from "@material-ui/core/ListSubheader";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";


class ImportDialogTemplate extends React.Component {
    render() {
        // i18n 組件
        const { formatMessage } = this.props.intl;
        const ERRR = () => {
            // 流程參數
            function getSteps() {
                return [
                    "匯入資料",
                    "資料檢核",
                    "檢核Excel分頁",
                    "檢核Excel欄位",
                    "檢核Excel格式",
                    "完成",
                    // formatMessage({
                    //     id:"dialog.importDialog.importData",
                    //     defaultMessage: "Import Dataecxa"
                    // }),
                    // formatMessage({
                    //     id:"dialog.importDialog.dataCheck",
                    //     defaultMessage: "Import Datafasd"
                    // }),
                    // formatMessage({
                    //     id:"home.overviewTest",
                    //     defaultMessage: "Import Datadfdf"
                    // }),
                    // formatMessage({
                    //     id:"home.work-order",
                    //     defaultMessage: "Import Datadede"
                    // }),
                    // formatMessage({
                    //     id:"dialog.importDialog.success",
                    //     defaultMessage: "Import Data"
                    // }),
                ];
            }
            // Step 基本參數
            const [activeStep, setActiveStep] = React.useState(0);
            const steps = getSteps();
            const [skipped, setSkipped] = React.useState(new Set());

            const handleNext = (steps) => {
                setActiveStep((prevActiveStep) => steps);
            };

            const handleBack = () => {
                setActiveStep((prevActiveStep) => prevActiveStep - 1);
            };

            const handleReset = () => {
                setActiveStep(0);
            };
            // 匯入資料設置
            const reducer = (state, action) => {
                switch (action.type) {
                    case 'SET_DROP_DEPTH':
                        return { ...state, dropDepth: action.dropDepth }
                    case 'SET_IN_DROP_ZONE':
                        return { ...state, inDropZone: action.inDropZone };
                    case 'ADD_FILE_TO_LIST':
                        return { ...state, fileList: state.fileList.concat(action.files) };
                    default:
                        return state;
                }
            };

            const [data, dispatch] = React.useReducer(
                reducer, { dropDepth: 0, inDropZone: false, fileList: [] }

            )
            // Excel Files 初始化
            let excelFilesData = [];

            const setExcelFilesData = props => {
                excelFilesData = props
            };

            let checkExcelFilesIfNull = false;
            const setCheckExcelFilesIfNull = props => {
                checkExcelFilesIfNull = props
            }
            // 對照表有錯誤物件
            const [errorObj, setErrorObj] = React.useState({});

            const handleErrorObj = (obj) => {
                setErrorObj(obj);
            };


            // 對照表開關
            function ImportSwitch() {
                console.log(errorObj)
                if( Object.keys(errorObj).length >= 1) {
                    return (
                        <div>
                            <TableContainer style={{ maxHeight: "340px" }}>
                                <Table stickyHeader aria-label="sticky table">
                                    <TableHead>
                                    </TableHead>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell className="center">
                                                尚未對應到系統所需名稱
                                            </TableCell>
                                            <TableCell className="center">
                                                尚未對應到匯入現有名稱
                                            </TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow>
                                            {
                                                errorObj.notFound.map((notFoundValue) => {
                                                return( <TableCell className="center" key={ notFoundValue }>{ notFoundValue }</TableCell> )
                                                })
                                            }
                                            <TableCell>
                                                <FormControl style={{ minWidth: 120 }}>
                                                    <Select defaultValue="" id="grouped-select">
                                                        {
                                                            errorObj.notCorrespond.map((notCorrespond) => {
                                                                return(<MenuItem className="center" value={ notCorrespond }>{ notCorrespond }</MenuItem>)
                                                            })
                                                        }
                                                    </Select>
                                                </FormControl>
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </div>
                    )
                } else {
                    return(
                        <DragAndDropWindow/>
                    )
                }

            }


            const DragAndDropWindow = props => {
                // Excel Files 賦值
                if( data !== null ){
                    setExcelFilesData(data)
                }

                if(data.fileList.length > 0) {
                    setCheckExcelFilesIfNull(true)
                }
                return(
                    <div>
                        <DragAndDrop data={ data } dispatch={ dispatch }/>
                        <ol className="dropped-files">
                            { data.fileList.map(f => {
                                return (
                                    <li key={f.name}>{f.name}</li>
                                )
                            })}
                        </ol>
                    </div>
                )
            }


            // 匯入流程
            function StepperTemplate() {

                return(
                    <div className="import-stepper">
                        <Stepper activeStep={ activeStep }>
                            { steps.map((label, index) =>{
                                const stepProps = {};
                                const labelProps = {};
                                return (
                                    <Step key={label} {...stepProps}>
                                        <StepLabel {...labelProps}>{label}</StepLabel>
                                    </Step>
                                );
                            })}
                        </Stepper>
                        <ImportSwitch />
                    </div>
                )
            }

            function dataValidation() {
                excelFilesData.fileList.map(file => {
                    const fileReader = new FileReader();
                    fileReader.onload = event => {
                        try {
                            const { result } = event.target;
                            // 以二进制流方式读取得到整份excel表格对象
                            const workbook = XLSX.read(result, { type: 'binary' });
                            CheckExcel(workbook)
                            // 存储获取到的数据
                            let data = [];
                            // 遍历每张工作表进行读取（这里默认只读取第一张表）
                            for (const sheet in workbook.Sheets) {
                                // esline-disable-next-line
                                if (workbook.Sheets.hasOwnProperty(sheet)) {
                                    // 利用 sheet_to_json 方法将 excel 转成 json 数据
                                    data = data.concat(XLSX.utils.sheet_to_json(workbook.Sheets[sheet]));
                                    // break; // 如果只取第一张表，就取消注释这行
                                }
                            }
                            // 最终获取到并且格式化后的 json 数据
                            console.log(data);
                        } catch (e) {
                            // 这里可以抛出文件类型错误不正确的相关提示
                        }
                    };
                    fileReader.readAsBinaryString(file);
                })
            }

            // 第一關 檢核Excel
            function CheckExcel(workbook) {
                handleNext(1);

                const checkSheetName = CheckSheetName(workbook);
                if( !checkSheetName.resultsVerification) {
                    handleErrorObj(CheckSheetName(workbook))
                }

            }

            // 第二關 檢核分頁名稱
            function CheckSheetName(workbook) {
                handleNext(2);

                // 確認結果
                const resultsVerification ={
                    resultsVerification: true,
                    message: '',
                    notFound: [],
                    notCorrespond:[]
                };
                // 未發現的分頁名稱
                let notFoundSheetName = []
                // 已對應到欄位
                let correspond = []
                // 未對應到欄位
                let notCorrespond = []
                // 系統所需要的分頁名稱
                const systemSheetName =
                    [
                        "workingdays",
                        "workinghours",
                        "resource_data",
                        "outsourcing_data",
                        "material_data",
                        "project_data",
                    ];
                // 檢核是否有缺少分頁
                systemSheetName.forEach( (sheetName) =>{

                    if ( workbook.SheetNames.indexOf(sheetName) >= 0){
                        correspond.push(sheetName)

                    } else {

                        resultsVerification.resultsVerification = false
                        notFoundSheetName.push(sheetName)

                    }
                })

                // 篩選未比對到的欄位
                workbook.SheetNames.forEach((sheetName) => {
                    if ( correspond.indexOf(sheetName) < 0){
                        notCorrespond.push(sheetName)

                    }
                })


                if( resultsVerification.resultsVerification ) {
                    handleNext(6);
                } else {
                    // 錯誤訊息
                    resultsVerification.message = '尚有未對應到分頁名稱:' + notFoundSheetName.toString()
                    // 尚未對應到系統所需分頁名稱
                    resultsVerification.notFound = notFoundSheetName
                    // 尚未對應到客戶現有分頁名稱
                    resultsVerification.notCorrespond = notCorrespond
                }
                return resultsVerification ;
            }


            // 檢核開始的Button
            function DataValidationBtn() {
                return(
                    <Button
                        disabled={ !checkExcelFilesIfNull }
                        onClick={ dataValidation }
                        color="primary"
                    >
                        <FormattedMessage
                            id="dialog.importDialog.dataCheck"
                            defaultMessage="Cancel"
                        />
                    </Button>
                )
            }

            // Next Button
            function NextBtn() {
                return(
                    <Button
                        onClick={ test }
                        color="primary"
                    >
                        <FormattedMessage
                            id="dialog.importDialog.nextButton"
                            defaultMessage="Next"
                        />
                    </Button>
                )
            }

            function test() {
                handleNext(6);
            }

            return(
                <div className="import-dialog">
                    <Dialog
                        disableBackdropClick
                        disableEscapeKeyDown
                        open={ this.props.openImportDialog }
                        onClose={ this.props.ImportDialogClose }
                    >
                        <DialogTitle>
                            <FormattedMessage
                                id="dialog.importDialog.importData"
                                defaultMessage="Cancel"
                            />
                        </DialogTitle>
                        <DialogContent>
                            <StepperTemplate />
                        </DialogContent>
                        <DialogActions>
                            <DataValidationBtn />
                            <NextBtn />
                            <Button onClick={ this.props.ImportDialogClose } color="primary">
                                <FormattedMessage
                                    id="dialog.importDialog.cancel"
                                    defaultMessage="Cancel"
                                />
                            </Button>
                        </DialogActions>
                    </Dialog>
                </div>
            )
        }

        return (
            <ERRR></ERRR>
        )
    }
}


const ImportDialog = injectIntl( ImportDialogTemplate );
export { ImportDialog }
