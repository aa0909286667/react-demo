import React from "react"
// import {  BryntumThemeCombo } from '@bryntum/gantt-react'

import 'core-js/stable';
import GanttTemplate from './components/Gantt';

class Gantt extends React.Component {
    render(){
        return(
            <div style={{ height: '650px' }}>
                <GanttTemplate/>
            </div>
            )
    }
}
export { Gantt }
