// 基本
import React from "react"
import { injectIntl, FormattedMessage } from 'react-intl';
// Card
import HomePageCard from "../../lib/HomePageCard";
// 主Table
import WorkOrderTable from "./element/work-order-table";


class WorkOrderTemplate extends React.Component {

    render(){
        // i18n
        const { formatMessage } = this.props.intl;

        const HomePageCardItems ={
            formatMessage: formatMessage,
            title: formatMessage({
                id: "home.work_order",
                defaultMessage: "Work Order"
            }),
            pageItems:[
                {
                    pageTitle: formatMessage({
                        id: "home.work_order_table",
                        defaultMessage: "Work Order Table"
                    }),
                    pageContent: <WorkOrderTable formatMessage={formatMessage} />,

                },
                {
                    pageTitle: formatMessage({
                        id: "home.gantt",
                        defaultMessage: "Gantt"
                    }),
                    pageContent: "Gantt",

                },
            ]
        }

        return(
            <div className="work-order">
                <HomePageCard
                    HomePageCardItems={ HomePageCardItems }
                />
            </div>
        )
    }
}

const WorkOrder = injectIntl( WorkOrderTemplate );

export { WorkOrder }
