import React from "react";
import Tables from "../../../../Table/Tables";
import { injectIntl } from "react-intl";
import ImportDataDialog from "../../../../Dialog/ImportDialog/ImportControl/ImportDataDialog";

// 控制匯入資料 dialog
function ImportDialog() {
    const [openImportDialog, setOpenImportDialog] = React.useState(false);

    const ImportDialogClose = () => {
        setOpenImportDialog(false);
    };

    const ImportDialogOpen = () => {
        setOpenImportDialog(true);
    };

    return(
        <ImportDataDialog
            openImportDialog={ openImportDialog }
            ImportDialogClose={ ImportDialogClose }
            ImportDialogOpen={ ImportDialogOpen }
        />
    )
}

const WorkOrderTableTemplate = props => {
    // 父頁面值
    const { formatMessage } = props;

    // Table Header
    const columns = [
        {
            id: 'order_id',
            label:
                formatMessage({
                    id:"work-order.order_id",
                    defaultMessage: "Order ID"
                }),
        },
        {
            id: 'project_id',
            label:
                formatMessage({
                    id:"work-order.project_id",
                    defaultMessage: "Project ID"
                }),
        },
        {
            id: 'part_number',
            label:
                formatMessage({
                    id:"work-order.part_number",
                    defaultMessage: "Part Number"
                }),
        },
        {
            id: 'project_type',
            label:
                formatMessage({
                    id:"work-order.project_type",
                    defaultMessage: "Project Type"
                }),
        },
        {
            id: 'order_date',
            label:
                formatMessage({
                    id:"work-order.order_date",
                    defaultMessage: "Order Date"
                }),
        },
        {
            id: 'due',
            label:
                formatMessage({
                    id:"work-order.due",
                    defaultMessage: "Due"
                }),
        },
        {
            id: 'plan_end_time',
            label:
                formatMessage({
                    id:"work-order.plan_end_time",
                    defaultMessage: "Plan End Time"
                }),
        },
        {
            id: 'status',
            label:
                formatMessage({
                    id:"work-order.status",
                    defaultMessage: "Status"
                }),
        },
    ];

    // Table Key
    function createData(order_id, project_id, part_number, project_type, order_date,due,plan_end_time,status) {
        return { order_id, project_id, part_number, project_type, order_date,due,plan_end_time,status };
    }

    // Table Items
    const rows = [
        // createData('Test', 159, 6.0, 24, 4.0, 2, 3, 5),
        // createData('Ice cream sandwich', 237, 9.0, 37, 4.3, 2, 3, 5),
        // createData('Eclair', 262, 16.0, 24, 6.0, 2, 3, 5),
        // createData('Cupcake', 305, 3.7, 67, 4.3, 2, 3, 5),
        // createData('Gingerbread', 356, 16.0, 49, 3.9, 2, 3, 5),
        // createData('Gingerbread', 356, 16.0, 49, 3.9, 2, 3, 5),
        // createData('Gingerbread', 356, 16.0, 49, 3.9, 2, 3, 5),
        // createData('Gingerbread', 356, 16.0, 49, 3.9, 2, 3, 5),
        // createData('Gingerbread', 356, 16.0, 49, 3.9, 2, 3, 5),
        // createData('Gingerbread', 356, 16.0, 49, 3.9, 2, 3, 5),
        // createData('Gingerbread', 356, 16.0, 49, 3.9, 2, 3, 5),
        // createData('Gingerbread', 356, 16.0, 49, 3.9, 2, 3, 5),
        // createData('Gingerbread', 356, 16.0, 49, 3.9, 2, 3, 5),
        // createData('Gingerbread', 356, 16.0, 49, 3.9, 2, 3, 5),
    ];

    return(
        <Tables
            rows={ rows }
            columns={ columns }
            formatMessage={ formatMessage }
            ImportDialog={ ImportDialog }
        />
    )
}

const WorkOrderTable = injectIntl( WorkOrderTableTemplate );
export default WorkOrderTable;
