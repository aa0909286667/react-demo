import React from "react";
import Tables from "../../../../Table/Tables";
import { injectIntl } from "react-intl";
import ImportDataDialog from "../../../../Dialog/ImportDialog/ImportControl/ImportDataDialog";

// 控制匯入資料 dialog
function ImportDialog() {
    const [openImportDialog, setOpenImportDialog] = React.useState(false);

    const ImportDialogClose = () => {
        setOpenImportDialog(false);
    };

    const ImportDialogOpen = () => {
        setOpenImportDialog(true);
    };

    return(
        <ImportDataDialog
            openImportDialog={ openImportDialog }
            ImportDialogClose={ ImportDialogClose }
            ImportDialogOpen={ ImportDialogOpen }
        />
    )
}

const ProcessTableTemplate = props => {
    // 父頁面值
    const { formatMessage } = props;

    // Table Header
    const columns = [
        {
            id: 'job_name',
            label:
                formatMessage({
                    id:"process.job_name",
                    defaultMessage: "Order ID"
                }),
        },
        {
            id: 'process_name',
            label:
                formatMessage({
                    id:"process.process_name",
                    defaultMessage: "Process Name"
                }),
        },
        {
            id: 'sourcing',
            label:
                formatMessage({
                    id:"process.sourcing",
                    defaultMessage: "Sourcing"
                }),
        },
        {
            id: 'resource_type',
            label:
                formatMessage({
                    id:"process.resource_type",
                    defaultMessage: "Resource Type"
                }),
        },
        {
            id: 'status',
            label:
                formatMessage({
                    id:"process.status",
                    defaultMessage: "Status"
                }),
        },
    ];

    // Table Key
    function createData(job_name, process_name, sourcing, resource_type, status) {
        return { job_name, process_name, sourcing, resource_type, status };
    }

    // Table Items
    const rows = [
        createData('Test', 159, 6.0, 24, 4.0),
        createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
        createData('Eclair', 262, 16.0, 24, 6.0),
        createData('Cupcake', 305, 3.7, 67, 4.3),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
    ];

    return(
        <Tables
            rows={ rows }
            columns={ columns }
            formatMessage={ formatMessage }
            ImportDialog={ ImportDialog }
        />
    )
}

const ProcessTable = injectIntl( ProcessTableTemplate );
export default ProcessTable;
