import React from "react";
import Tables from "../../../../Table/Tables";
import { injectIntl } from "react-intl";
import ImportDataDialog from "../../../../Dialog/ImportDialog/ImportControl/ImportDataDialog";

// 控制匯入資料 dialog
function ImportDialog() {
    const [openImportDialog, setOpenImportDialog] = React.useState(false);

    const ImportDialogClose = () => {
        setOpenImportDialog(false);
    };

    const ImportDialogOpen = () => {
        setOpenImportDialog(true);
    };

    return(
        <ImportDataDialog
            openImportDialog={ openImportDialog }
            ImportDialogClose={ ImportDialogClose }
            ImportDialogOpen={ ImportDialogOpen }
        />
    )
}

const EmployeeTableTemplate = props => {
    // 父頁面值
    const { formatMessage } = props;

    // Table Header
    const columns = [
        {
            id: 'employee_id',
            label:
                formatMessage({
                    id:"resources.employee.employee_id",
                    defaultMessage: "Employee ID"
                }),
        },
        {
            id: 'job_name',
            label:
                formatMessage({
                    id:"resources.employee.job_name",
                    defaultMessage: "Job Name"
                }),
        },
        {
            id: 'multitasking',
            label:
                formatMessage({
                    id:"resources.employee.multitasking",
                    defaultMessage: "Multitasking"
                }),
        },
        {
            id: 'calendar',
            label:
                formatMessage({
                    id:"resources.employee.calendar",
                    defaultMessage: "Calendar"
                }),
        },
        {
            id: 'working_hours',
            label:
                formatMessage({
                    id:"resources.employee.working_hours",
                    defaultMessage: "Working Hours"
                }),
        },
    ];

    // Table Key
    function createData(employee_id, job_name, multitasking, calendar, working_hours) {
        return { employee_id, job_name, multitasking, calendar, working_hours };
    }

    // Table Items
    const rows = [
        createData('Test', 159, 6.0, 24, 4.0),
        createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
        createData('Eclair', 262, 16.0, 24, 6.0),
        createData('Cupcake', 305, 3.7, 67, 4.3, 2, 3, 5),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
        createData('Gingerbread', 356, 16.0, 49, 3.9),
    ];

    return(
        <Tables
            rows={ rows }
            columns={ columns }
            formatMessage={ formatMessage }
            ImportDialog={ ImportDialog }
        />
    )
}

const EmployeeTable = injectIntl( EmployeeTableTemplate );
export default EmployeeTable;
