// 基本
import React from "react";
import { injectIntl } from "react-intl";
// Table
import Tables from "../../../../Table/Tables";
// Button
import Button from "@material-ui/core/Button";
// icon
import PublishIcon from '@material-ui/icons/Publish';
import GetAppIcon from '@material-ui/icons/GetApp';


const AddResourcesTemplate = props => {
    // 父頁面值
    const { formatMessage } = props;

    // Table Header
    const columns = [
        {
            id: 'employee_id',
            label:
                formatMessage({
                    id:"resources.employee.employee_id",
                    defaultMessage: "Employee ID"
                }),
        },
        {
            id: 'job_name',
            label:
                formatMessage({
                    id:"resources.employee.job_name",
                    defaultMessage: "Job Name"
                }),
        },
        {
            id: 'multitasking',
            label:
                formatMessage({
                    id:"resources.employee.multitasking",
                    defaultMessage: "Multitasking"
                }),
        },
        {
            id: 'calendar',
            label:
                formatMessage({
                    id:"resources.employee.calendar",
                    defaultMessage: "Calendar"
                }),
        },
        {
            id: 'working_hours',
            label:
                formatMessage({
                    id:"resources.employee.working_hours",
                    defaultMessage: "Working Hours"
                }),
        },
    ];

    // Table Key
    function createData(employee_id, job_name, multitasking, calendar, working_hours) {
        return { employee_id, job_name, multitasking, calendar, working_hours };
    }

    // Table Items
    const rows = [
        // createData('Test', 159, 6.0, 24, 4.0),
    ];

    return(
        <div>
            <div className="float-right">
                <Button
                    variant="contained"
                    color="secondary"
                    startIcon={ <PublishIcon /> }
                >
                    匯入資料
                </Button>
                <Button
                    variant="contained"
                    color="secondary"
                    startIcon={ <GetAppIcon /> }
                >
                    下載範本
                </Button>
            </div>

            <Tables
                rows={ rows }
                columns={ columns }
                formatMessage={ formatMessage }
            />
        </div>

    )
}

const AddResources = injectIntl( AddResourcesTemplate );
export default AddResources;
