import React, { Component } from 'react';
import '../../assets/css/card/job-description-card.scss';


class jobDescriptionCard extends Component {
    render() {
        return (
            <div className="job-description-card">
                <h3 className="center f-20">RCID0-H001</h3>
                <center>
                    <a className="btn btn-square-solid-secondary f-14 padding-top-and-bottom-4">
                        待進行工作
                    </a>
                </center>
            </div>
        );
    }
}

export default jobDescriptionCard;
