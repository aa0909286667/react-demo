// 基本
import React from "react";
import { HashRouter, Link } from "react-router-dom"
// Logo img
import logo from "../../../assets/images/sara.svg";
// icon
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCog, faUserCircle, faWindowRestore } from '@fortawesome/free-solid-svg-icons'
import Translate from '@material-ui/icons/Translate';
// Select "MenuItem" 是選單內容
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
// 按鈕
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';


function Header({ lang, setLang }) {


    const [open, setOpen] = React.useState(false);


    const handleClose = () => {
        setOpen(false);
    };

    const handleOpen = () => {
        setOpen(true);
    };

    return (

        <header className="header">
            <HashRouter>
                <Link to="/"><img src={ logo } className="App-logo" alt="logo" /></Link>
                <div className="right-features">
                    <FormControl className="translate">
                        <Button onClick={ handleOpen }>
                            <Translate />
                        </Button>
                        <Select
                            open={ open }
                            value={ lang }
                            onClose={handleClose}
                            onOpen={handleOpen}
                            onChange={(evt) => {
                                setLang(evt.target.value);
                            }}
                        >
                            <MenuItem value={ "en" }>English</MenuItem>
                            <MenuItem value={ "cn" }>中文</MenuItem>
                        </Select>
                    </FormControl>
                    <FontAwesomeIcon className="btn" icon={ faWindowRestore } />
                    <FontAwesomeIcon className="btn" icon={ faCog } />
                    <FontAwesomeIcon className="btn" icon={ faUserCircle } />
                </div>
            </HashRouter>
        </header>
    );
}

export { Header };
