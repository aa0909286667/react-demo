import React, { Component } from 'react';


class SaraBtn extends Component {
    render() {
        return (
            <div>
                <button className="btn btn-oval-primary ">Test Button</button>
                <button className="btn btn-oval-secondary ">Test Button</button>
                <button className="btn btn-oval-success ">Test Button</button>
                <button className="btn btn-oval-danger ">Test Button</button>
                <button className="btn btn-oval-warning ">Test Button</button>
                <button className="btn btn-oval-info ">Test Button</button>
                <button className="btn btn-oval-light ">Test Button</button>
                <button className="btn btn-oval-dark ">Test Button</button>
                <button className="btn btn-square-solid-dark">Button Dark</button>

                <button className="btn btn-square-primary ">Test Button</button>
                <button className="btn btn-square-secondary ">Test Button</button>
                <button className="btn btn-square-success ">Test Button</button>
                <button className="btn btn-square-danger ">Test Button</button>
                <button className="btn btn-square-warning ">Test Button</button>
                <button className="btn btn-square-light ">Test Button</button>
                <button className="btn btn-square-info ">Test Button</button>
                <button className="btn btn-square-dark ">Test Button</button>
            </div>
        );
    }
}

export default SaraBtn;
