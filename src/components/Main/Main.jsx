import React from "react"
import { HashRouter, Route } from "react-router-dom"
import { Home } from "../Home"
import { TestingPlatform } from "../TestingPlatform";

class Main extends React.Component {
    render() {
        return (
            <HashRouter>
                <div className="main">
                    <Route exact path="/" component={ Home } />
                    <Route exact path="/testing-platform" component={ TestingPlatform } />
                </div>
            </HashRouter>
        )
    }
}

export { Main }
