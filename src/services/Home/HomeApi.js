import axios from "../request"

const overviewData = 'api/get_schedule_progress';
const test = '/courses/fundraising';

export default {

    async getOverviewData() {
        const result = await axios.get(overviewData);
        return result;
    },

}
